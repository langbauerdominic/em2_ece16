 #include <HAL/hal_pwm.h>

 
void HAL_PWM_Init(void)
{
	//period_count = (1/(desired pwm freq) / (1/(master clock/8))
	unsigned int period_count = SysCtlClockGet()/(8*PWM_FREQ); //period val

	SysCtlPWMClockSet(SYSCTL_PWMDIV_8); //clock divider

	//Configure PA6 & PA7 Pins as PWM
	GPIOPinConfigure(GPIO_PA6_M1PWM2);
	GPIOPinConfigure(GPIO_PA7_M1PWM3);

	PWMGenConfigure(PWM1_BASE, PWM_GEN_1, PWM_GEN_MODE_DOWN | PWM_GEN_MODE_NO_SYNC); //throttle & steering

	PWMGenPeriodSet(PWM1_BASE, PWM_GEN_1, period_count);

	PWMPulseWidthSet(PWM1_BASE, PWM_STEERING,0);
    PWMPulseWidthSet(PWM1_BASE, PWM_THROTTLE,0);

    PWMGenEnable(PWM1_BASE, PWM_GEN_1);

    PWMOutputState(PWM1_BASE, PWM_OUT_2_BIT | PWM_OUT_3_BIT, false);
}

void HAL_PWMSetSteering(int val)
{
	if(val)
	{

		PWMPulseWidthSet(PWM1_BASE, PWM_STEERING,val);
		PWMOutputState(PWM1_BASE, PWM_OUT_2_BIT, true);
	}
	else //if val is 0, stop pwm
		PWMOutputState(PWM1_BASE, PWM_OUT_2_BIT, false);
		
}

void HAL_PWMSetThrottle(int val)
{
	if(val)
	{
		PWMOutputState(PWM1_BASE, PWM_OUT_3_BIT, true);
		PWMPulseWidthSet(PWM1_BASE, PWM_THROTTLE,val);
	}
	else
		PWMOutputState(PWM1_BASE, PWM_OUT_3_BIT, false);
}

int HAL_PWMThrottleSetupPulses(int val)
{
	//should send a defined number of pulses 
	//i don't know if we need this with the new controller
	return 0;
}
