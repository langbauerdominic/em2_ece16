/*
 * hal_ucs.h
 *
 *  Created on: 14.03.2017
 *      Author: daniel
 */

#ifndef HAL_HAL_UCS_H_
#define HAL_HAL_UCS_H_


#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"

void HAL_UCS_Init();

#endif /* HAL_HAL_UCS_H_ */
