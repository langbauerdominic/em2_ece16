/*
 * hal_uart.h
 *
 *  Created on: Mar 21, 2017
 *      Author: daniel
 */

#ifndef HAL_HAL_UART_H_
#define HAL_HAL_UART_H_

#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "driverlib/ssi.h"
#include "driverlib/uart.h"
#include "utils/uartstdio.h"
#include "driverlib/pin_map.h"


void HAL_InitUart();


#endif /* HAL_HAL_UART_H_ */
