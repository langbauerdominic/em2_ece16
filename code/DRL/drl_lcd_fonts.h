/*
 * drl_lcd_fonts.h
 *
 *  Created on: Nov 19, 2016
 *      Author: daniel
 */

#ifndef DRL_DRL_LCD_FONTS_H_
#define DRL_DRL_LCD_FONTS_H_



const unsigned char font[][6] =
{
		{0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, // sp
		{0x0, 0x0, 0xf4, 0x0, 0x0, 0x0},  // !
		{0x0, 0xe0, 0x0, 0xe0, 0x0, 0x0},  // "
		{0x28, 0xfe, 0x28, 0xfe, 0x28, 0x0},  // #
		{0x24, 0x54, 0xfe, 0x54, 0x48, 0x0},  // $
		{0x46, 0x26, 0x10, 0xc8, 0xc4, 0x0},  // %
		{0x6c, 0x92, 0xaa, 0x44, 0xa, 0x0},  // &
		{0x0, 0xa0, 0xc0, 0x0, 0x0, 0x0},  // '
		{0x0, 0x38, 0x44, 0x82, 0x0, 0x0},  // (
		{0x0, 0x82, 0x44, 0x38, 0x0, 0x0},  // )
		{0x28, 0x10, 0x7c, 0x10, 0x28, 0x0},  // *
		{0x10, 0x10, 0x7c, 0x10, 0x10, 0x0},  // +
		{0x0, 0x0, 0xa, 0xc, 0x0, 0x0},  // ,
		{0x10, 0x10, 0x10, 0x10, 0x10, 0x0},  // -
		{0x0, 0x6, 0x6, 0x0, 0x0, 0x0},  // .
		{0x4, 0x8, 0x10, 0x20, 0x40, 0x0},  // /
		{0x7c, 0x8a, 0x92, 0xa2, 0x7c, 0x0},  // 0
		{0x0, 0x42, 0xfe, 0x2, 0x0, 0x0},  // 1
		{0x42, 0x86, 0x8a, 0x92, 0x62, 0x0},  // 2
		{0x84, 0x82, 0xa2, 0xd2, 0x8c, 0x0},  // 3
		{0x18, 0x28, 0x48, 0xfe, 0x8, 0x0},  // 4
		{0xe4, 0xa2, 0xa2, 0xa2, 0x9c, 0x0},  // 5
		{0x3c, 0x52, 0x92, 0x92, 0xc, 0x0},  // 6
		{0x80, 0x8e, 0x90, 0xa0, 0xc0, 0x0},  // 7
		{0x6c, 0x92, 0x92, 0x92, 0x6c, 0x0},  // 8
		{0x60, 0x92, 0x92, 0x94, 0x78, 0x0},  // 9
		{0x0, 0x6c, 0x6c, 0x0, 0x0, 0x0},  // :
		{0x0, 0x6a, 0x6c, 0x0, 0x0, 0x0},  // ;
		{0x10, 0x28, 0x44, 0x82, 0x0, 0x0},  // <
		{0x28, 0x28, 0x28, 0x28, 0x28, 0x0},  // =
		{0x0, 0x82, 0x44, 0x28, 0x10, 0x0},  // >
		{0x40, 0x80, 0x8a, 0x90, 0x60, 0x0},  // ?
		{0x4c, 0x92, 0x9a, 0x8a, 0x7c, 0x0},  // @
		{0x3e, 0x48, 0x88, 0x48, 0x3e, 0x0},  // A
		{0xfe, 0x92, 0x92, 0x92, 0x6c, 0x0},  // B
		{0x7c, 0x82, 0x82, 0x82, 0x44, 0x0},  // C
		{0xfe, 0x82, 0x82, 0x44, 0x38, 0x0},  // D
		{0xfe, 0x92, 0x92, 0x92, 0x82, 0x0},  // E
		{0xfe, 0x90, 0x90, 0x90, 0x80, 0x0},  // F
		{0x7c, 0x82, 0x92, 0x92, 0x5e, 0x0},  // G
		{0xfe, 0x10, 0x10, 0x10, 0xfe, 0x0},  // H
		{0x0, 0x82, 0xfe, 0x82, 0x0, 0x0},  // I
		{0x4, 0x2, 0x82, 0xfc, 0x80, 0x0},  // J
		{0xfe, 0x10, 0x28, 0x44, 0x82, 0x0},  // K
		{0xfe, 0x2, 0x2, 0x2, 0x2, 0x0},  // L
		{0xfe, 0x40, 0x30, 0x40, 0xfe, 0x0},  // M
		{0xfe, 0x20, 0x10, 0x8, 0xfe, 0x0},  // N
		{0x7c, 0x82, 0x82, 0x82, 0x7c, 0x0},  // O
		{0xfe, 0x90, 0x90, 0x90, 0x60, 0x0},  // P
		{0x7c, 0x82, 0x8a, 0x84, 0x7a, 0x0},  // Q
		{0xfe, 0x90, 0x98, 0x94, 0x62, 0x0},  // R
		{0x62, 0x92, 0x92, 0x92, 0x8c, 0x0},  // S
		{0x80, 0x80, 0xfe, 0x80, 0x80, 0x0},  // T
		{0xfc, 0x2, 0x2, 0x2, 0xfc, 0x0},  // U
		{0xf8, 0x4, 0x2, 0x4, 0xf8, 0x0},  // V
		{0xfc, 0x2, 0x1c, 0x2, 0xfc, 0x0},  // W
		{0xc6, 0x28, 0x10, 0x28, 0xc6, 0x0},  // X
		{0xe0, 0x10, 0xe, 0x10, 0xe0, 0x0},  // Y
		{0x86, 0x8a, 0x92, 0xa2, 0xc2, 0x0},  // Z
		{0x0, 0xfe, 0x82, 0x82, 0x0, 0x0},  // [
		{0x20, 0x10, 0x8, 0x4, 0x2, 0x0},  // /
		{0x0, 0x82, 0x82, 0xfe, 0x0, 0x0},  // ]
		{0x20, 0x40, 0x80, 0x40, 0x20, 0x0},  // ^
		{0x2, 0x2, 0x2, 0x2, 0x2, 0x0},  // _
		{0x0, 0x80, 0x40, 0x20, 0x0, 0x0},  // '
		{0x4, 0x2a, 0x2a, 0x2a, 0x1e, 0x0},  // a
		{0xfe, 0x12, 0x22, 0x22, 0x1c, 0x0},  // b
		{0x1c, 0x22, 0x22, 0x22, 0x4, 0x0},  // c
		{0x1c, 0x22, 0x22, 0x12, 0xfe, 0x0},  // d
		{0x1c, 0x2a, 0x2a, 0x2a, 0x18, 0x0},  // e
		{0x10, 0x7e, 0x90, 0x80, 0x40, 0x0},  // f
		{0x18, 0x25, 0x25, 0x25, 0x3e, 0x0},  // g
		{0xfe, 0x10, 0x20, 0x20, 0x1e, 0x0},  // h
		{0x0, 0x22, 0xbe, 0x2, 0x0, 0x0},  // i
		{0x2, 0x1, 0x21, 0xbe, 0x0, 0x0},  // j
		{0xfe, 0x8, 0x14, 0x22, 0x0, 0x0},  // k
		{0x0, 0x82, 0xfe, 0x2, 0x0, 0x0},  // l
		{0x3e, 0x20, 0x18, 0x20, 0x1e, 0x0},  // m
		{0x3e, 0x10, 0x20, 0x20, 0x1e, 0x0},  // n
		{0x1c, 0x22, 0x22, 0x22, 0x1c, 0x0},  // o
		{0x3f, 0x24, 0x24, 0x24, 0x18, 0x0},  // p
		{0x18, 0x24, 0x24, 0x18, 0x3f, 0x0},  // q
		{0x3e, 0x10, 0x20, 0x20, 0x10, 0x0},  // r
		{0x12, 0x2a, 0x2a, 0x2a, 0x4, 0x0},  // s
		{0x20, 0xfc, 0x22, 0x2, 0x4, 0x0},  // t
		{0x3c, 0x2, 0x2, 0x4, 0x3e, 0x0},  // u
		{0x38, 0x4, 0x2, 0x4, 0x38, 0x0},  // v
		{0x3c, 0x2, 0xc, 0x2, 0x3c, 0x0},  // w
		{0x22, 0x14, 0x8, 0x14, 0x22, 0x0},  // x
		{0x38, 0x5, 0x5, 0x5, 0x3e, 0x0},  // y
		{0x22, 0x26, 0x2a, 0x32, 0x22, 0x0},  // z
		{0x10, 0x6c, 0x82, 0x82, 0x0, 0x0},  // {
		{0x0, 0x0, 0xfe, 0x0, 0x0, 0x0},  // |
		{0x82, 0x82, 0x6c, 0x10, 0x0, 0x0},  // }
		{0x40, 0x20, 0x40, 0x80, 0x40, 0x0}    // ~
};



#endif /* DRL_DRL_LCD_FONTS_H_ */
