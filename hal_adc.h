/*
 * hal_adc.h
 *
 *  Created on: 27.04.2018
 *      Author: Dominic Langbauer
 */

#ifndef HAL_HAL_ADC_H_
#define HAL_HAL_ADC_H_

void HAL_ADC_INIT();
void SAMPLE_ADC();


#endif /* HAL_HAL_ADC_H_ */
